SHELL:=/bin/bash
.DEFAULT_GOAL := rpm

VERSION = 0.1.0
RELEASE = 1
ARCH = x86_64
RPM_NAME = cactus-vm-exporters-${VERSION}-${RELEASE}.${ARCH}.rpm

VERSION_NODE_EXPORTER = 0.18.1
ARCH_NODE_EXPORTER = amd64

.PHONY: rpm
rpm: ${RPM_NAME}

node_exporter:
	curl -OL https://github.com/prometheus/node_exporter/releases/download/v${VERSION_NODE_EXPORTER}/node_exporter-${VERSION_NODE_EXPORTER}.linux-${ARCH_NODE_EXPORTER}.tar.gz
	tar xf node_exporter-${VERSION_NODE_EXPORTER}.linux-${ARCH_NODE_EXPORTER}.tar.gz
	mv node_exporter-${VERSION_NODE_EXPORTER}.linux-${ARCH_NODE_EXPORTER}/node_exporter .
	rm -rf node_exporter-${VERSION_NODE_EXPORTER}.linux-${ARCH_NODE_EXPORTER}.tar.gz node_exporter-${VERSION_NODE_EXPORTER}.linux-${ARCH_NODE_EXPORTER}

${RPM_NAME}: node_exporter
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus/vm-exporters rpmroot/usr/lib/systemd/system
	cp systemd/* rpmroot/usr/lib/systemd/system
	cp $< rpmroot/opt/cactus/vm-exporters

	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-vm-exporters \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--iteration ${RELEASE} \
	--vendor CERN \
	--description "Prometheus exporters for L1 Online Software VMs at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/vm-exporters" \
	--provides cactus_vm_exporters \
	.=/ && mv *.rpm ..